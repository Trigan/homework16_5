//#include <iostream>
//
//int main()
//{
//	const int N = 6;
//	int array[N][N] = { {1},{1} };
//
//	for (int i = 0; i < N; i++)
//	{
//		for (int j = 0; j < N; j++)
//		{
//			std::cout << array[i][j];
//		}
//		std::cout << "\n";
//	}
//
//	int i, j, N[4][4];
//	for (i = 0; i < 4; ++i) 
//	{
//		for (j = 0; j < 4; ++j)
//		{
//			N[i][j] = (i * 4) + j + 1;
//			std::cout << N[i][j] << " ";
//		}
//		std::cout << "\n";
//	}
//}


#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;
int const N = 5;

int main()
{
	int n = 6, sum;
	int A[N][N];
	time_t t = time(0);
	tm Localtime;
	time(&t);
	int d = (localtime(&t)->tm_mday) % n;

	int IndexSum;
	int Sum = 0;

	const int x = 4;
	const int y = 6;
	sum = 0;

	int i, j, arr[x][y];

	setlocale(LC_CTYPE, "rus");
	localtime_s(&Localtime, &t);
	IndexSum = (&Localtime)->tm_mday % N;

	for (int i = 0; i < N; i++)
	{

		for (int j = 0; j < N; j++)
		{
			A[i][j] = i + j;
			cout << A[i][j] << " ";
			if (i == d) sum += arr[i][j];
		}
		cout << "\n";

	}

	cout << "Sum of row " << IndexSum << ": ";

	for (int i = 0; i < N; i++)
	{
		Sum += A[IndexSum][i];

	}
	cout << Sum << "\n";
}
